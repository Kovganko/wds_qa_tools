﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace updater
{
    public partial class UpdaterForm : Form
    {
        public UpdaterForm()
        {
            InitializeComponent();
        }
        private string updateUrl = "http://download.pershastudia.org/WDS%20QA%20Tools/wds_qa_tools.exe";
        private string version_txt = "http://download.pershastudia.org/wot/patches/kovganko1/wds_qa_tools/version.txt";
        private string exe = Path.Combine(Path.GetFullPath("updates"), "wds_qa_tools.exe");
        private void UpdaterForm_Load(object sender, EventArgs e)
        {
            Process[] qa_tools = Process.GetProcessesByName("wds_qa_tools");
            if (qa_tools.Length > 0)
            {
                qa_tools[0].Kill();
            }
            this.progressBar1.Value += 3;
            
            string version = null;
            using (var client = new WebClient())
            {
                try
                {
                    version = client.DownloadString(version_txt);
                }
                catch (WebException)
                {

                }
            }
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            string app_version = fvi.FileVersion;
            if (version != null && version != app_version)
            {
                start_update();
            }
            else
            {
                this.Close();
            }
            

        }
        private void start_update()
        {
            
            if (!Directory.Exists(Path.GetFullPath("updates")))
            {
                Directory.CreateDirectory(Path.GetFullPath("updates"));
            }
            if (File.Exists(exe))
            {
                File.Delete(exe);
            }
            this.progressBar1.Value += 12;
            Thread thread = new Thread(() => {
                using (var client = new WebClient())
                {
                    try
                    {
                        client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
                        client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
                        client.DownloadFileAsync(new Uri(updateUrl), exe);
                    }
                    catch (WebException)
                    {

                    }
                }
            });
            thread.Start();
            
            
        }
        private void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            this.BeginInvoke((MethodInvoker)delegate {
                double bytesIn = double.Parse(e.BytesReceived.ToString());
                double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
                double percentage = bytesIn / totalBytes * 100;
                progressBar1.Value = int.Parse(Math.Truncate(percentage).ToString());
            });
        }
        private void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            this.BeginInvoke((MethodInvoker)delegate {
                File.Delete(Path.GetFullPath("wds_qa_tools.exe"));
                File.Move(exe, Path.GetFullPath("wds_qa_tools.exe"));
                Process.Start("wds_qa_tools.exe");
                this.Close();
            });
        }
    }
}
