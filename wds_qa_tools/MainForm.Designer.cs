﻿namespace wds_qa_tools
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mocks = new System.Windows.Forms.TabControl();
            this.tabDisk = new System.Windows.Forms.TabPage();
            this.diskName = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pathcontains = new System.Windows.Forms.CheckBox();
            this.containstextbox = new System.Windows.Forms.TextBox();
            this.buttonUnlock = new System.Windows.Forms.Button();
            this.checkBoxDir = new System.Windows.Forms.CheckBox();
            this.checkBoxFile = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lockButton = new System.Windows.Forms.Button();
            this.filePathToLock = new System.Windows.Forms.TextBox();
            this.freeSpace = new System.Windows.Forms.Button();
            this.fillSpace = new System.Windows.Forms.Button();
            this.diskNameLabel = new System.Windows.Forms.Label();
            this.tabLogs = new System.Windows.Forms.TabPage();
            this.perflauncher = new System.Windows.Forms.Button();
            this.geterrors = new System.Windows.Forms.Button();
            this.savefile = new System.Windows.Forms.Button();
            this.wgcLog = new System.Windows.Forms.Label();
            this.perfbtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.networktab = new System.Windows.Forms.TabPage();
            this.uploadspeedlabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.downloadspeedlabel = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.adapters = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.turnonbtn = new System.Windows.Forms.Button();
            this.turnoffbtn = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.enablepreloadpatches = new System.Windows.Forms.CheckBox();
            this.custommock = new System.Windows.Forms.Button();
            this.createversion = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.wgcbuild = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.wgcbranch = new System.Windows.Forms.TextBox();
            this.wgcinfo = new System.Windows.Forms.Button();
            this.ips = new System.Windows.Forms.ComboBox();
            this.disablemock = new System.Windows.Forms.Button();
            this.startmock = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.killp = new System.Windows.Forms.Button();
            this.priorities = new System.Windows.Forms.ComboBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.setpriority = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.procesess = new System.Windows.Forms.ComboBox();
            this.output = new System.Windows.Forms.RichTextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.mocks.SuspendLayout();
            this.tabDisk.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabLogs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.networktab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // mocks
            // 
            this.mocks.Controls.Add(this.tabDisk);
            this.mocks.Controls.Add(this.tabLogs);
            this.mocks.Controls.Add(this.networktab);
            this.mocks.Controls.Add(this.tabPage1);
            this.mocks.Controls.Add(this.tabPage2);
            this.mocks.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mocks.Location = new System.Drawing.Point(12, 12);
            this.mocks.Name = "mocks";
            this.mocks.SelectedIndex = 0;
            this.mocks.Size = new System.Drawing.Size(836, 339);
            this.mocks.TabIndex = 0;
            // 
            // tabDisk
            // 
            this.tabDisk.Controls.Add(this.diskName);
            this.tabDisk.Controls.Add(this.label9);
            this.tabDisk.Controls.Add(this.pathcontains);
            this.tabDisk.Controls.Add(this.containstextbox);
            this.tabDisk.Controls.Add(this.buttonUnlock);
            this.tabDisk.Controls.Add(this.checkBoxDir);
            this.tabDisk.Controls.Add(this.checkBoxFile);
            this.tabDisk.Controls.Add(this.pictureBox1);
            this.tabDisk.Controls.Add(this.lockButton);
            this.tabDisk.Controls.Add(this.filePathToLock);
            this.tabDisk.Controls.Add(this.freeSpace);
            this.tabDisk.Controls.Add(this.fillSpace);
            this.tabDisk.Controls.Add(this.diskNameLabel);
            this.tabDisk.Location = new System.Drawing.Point(4, 26);
            this.tabDisk.Name = "tabDisk";
            this.tabDisk.Padding = new System.Windows.Forms.Padding(3);
            this.tabDisk.Size = new System.Drawing.Size(828, 309);
            this.tabDisk.TabIndex = 0;
            this.tabDisk.Text = "Disk Usage";
            this.tabDisk.UseVisualStyleBackColor = true;
            // 
            // diskName
            // 
            this.diskName.FormattingEnabled = true;
            this.diskName.Location = new System.Drawing.Point(33, 52);
            this.diskName.Name = "diskName";
            this.diskName.Size = new System.Drawing.Size(80, 25);
            this.diskName.TabIndex = 14;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(369, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 17);
            this.label9.TabIndex = 13;
            this.label9.Text = "Open";
            // 
            // pathcontains
            // 
            this.pathcontains.AutoSize = true;
            this.pathcontains.Location = new System.Drawing.Point(273, 106);
            this.pathcontains.Name = "pathcontains";
            this.pathcontains.Size = new System.Drawing.Size(77, 21);
            this.pathcontains.TabIndex = 12;
            this.pathcontains.Text = "Contains";
            this.pathcontains.UseVisualStyleBackColor = true;
            this.pathcontains.CheckedChanged += new System.EventHandler(this.pathcontains_CheckedChanged);
            // 
            // containstextbox
            // 
            this.containstextbox.Enabled = false;
            this.containstextbox.Location = new System.Drawing.Point(407, 88);
            this.containstextbox.Name = "containstextbox";
            this.containstextbox.Size = new System.Drawing.Size(395, 25);
            this.containstextbox.TabIndex = 11;
            // 
            // buttonUnlock
            // 
            this.buttonUnlock.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonUnlock.Location = new System.Drawing.Point(647, 141);
            this.buttonUnlock.Name = "buttonUnlock";
            this.buttonUnlock.Size = new System.Drawing.Size(145, 38);
            this.buttonUnlock.TabIndex = 10;
            this.buttonUnlock.Text = "Unlock";
            this.buttonUnlock.UseVisualStyleBackColor = true;
            this.buttonUnlock.Click += new System.EventHandler(this.buttonUnlock_Click);
            // 
            // checkBoxDir
            // 
            this.checkBoxDir.AutoSize = true;
            this.checkBoxDir.Location = new System.Drawing.Point(274, 79);
            this.checkBoxDir.Name = "checkBoxDir";
            this.checkBoxDir.Size = new System.Drawing.Size(80, 21);
            this.checkBoxDir.TabIndex = 9;
            this.checkBoxDir.Text = "Directory";
            this.checkBoxDir.UseVisualStyleBackColor = true;
            this.checkBoxDir.CheckedChanged += new System.EventHandler(this.chk_Click);
            // 
            // checkBoxFile
            // 
            this.checkBoxFile.AutoSize = true;
            this.checkBoxFile.Checked = true;
            this.checkBoxFile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxFile.Location = new System.Drawing.Point(273, 52);
            this.checkBoxFile.Name = "checkBoxFile";
            this.checkBoxFile.Size = new System.Drawing.Size(46, 21);
            this.checkBoxFile.TabIndex = 8;
            this.checkBoxFile.Text = "File";
            this.checkBoxFile.UseVisualStyleBackColor = true;
            this.checkBoxFile.CheckedChanged += new System.EventHandler(this.chk_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Image = global::wds_qa_tools.Properties.Resources.open;
            this.pictureBox1.Location = new System.Drawing.Point(372, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(29, 25);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // lockButton
            // 
            this.lockButton.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lockButton.Location = new System.Drawing.Point(467, 142);
            this.lockButton.Name = "lockButton";
            this.lockButton.Size = new System.Drawing.Size(148, 37);
            this.lockButton.TabIndex = 6;
            this.lockButton.Text = "Lock";
            this.lockButton.UseVisualStyleBackColor = true;
            this.lockButton.Click += new System.EventHandler(this.lockButton_Click);
            // 
            // filePathToLock
            // 
            this.filePathToLock.Location = new System.Drawing.Point(407, 48);
            this.filePathToLock.Name = "filePathToLock";
            this.filePathToLock.Size = new System.Drawing.Size(395, 25);
            this.filePathToLock.TabIndex = 5;
            // 
            // freeSpace
            // 
            this.freeSpace.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.freeSpace.Location = new System.Drawing.Point(33, 141);
            this.freeSpace.Name = "freeSpace";
            this.freeSpace.Size = new System.Drawing.Size(153, 34);
            this.freeSpace.TabIndex = 3;
            this.freeSpace.Text = "Free Space";
            this.freeSpace.UseVisualStyleBackColor = true;
            this.freeSpace.Click += new System.EventHandler(this.freeSpace_Click);
            // 
            // fillSpace
            // 
            this.fillSpace.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fillSpace.Location = new System.Drawing.Point(33, 97);
            this.fillSpace.Name = "fillSpace";
            this.fillSpace.Size = new System.Drawing.Size(150, 34);
            this.fillSpace.TabIndex = 1;
            this.fillSpace.Text = "Fill Space";
            this.fillSpace.UseVisualStyleBackColor = true;
            this.fillSpace.Click += new System.EventHandler(this.fillSpace_Click);
            // 
            // diskNameLabel
            // 
            this.diskNameLabel.AutoSize = true;
            this.diskNameLabel.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.diskNameLabel.Location = new System.Drawing.Point(37, 24);
            this.diskNameLabel.Name = "diskNameLabel";
            this.diskNameLabel.Size = new System.Drawing.Size(38, 23);
            this.diskNameLabel.TabIndex = 0;
            this.diskNameLabel.Text = "Disk";
            // 
            // tabLogs
            // 
            this.tabLogs.Controls.Add(this.perflauncher);
            this.tabLogs.Controls.Add(this.geterrors);
            this.tabLogs.Controls.Add(this.savefile);
            this.tabLogs.Controls.Add(this.wgcLog);
            this.tabLogs.Controls.Add(this.perfbtn);
            this.tabLogs.Controls.Add(this.label5);
            this.tabLogs.Controls.Add(this.pictureBox2);
            this.tabLogs.Location = new System.Drawing.Point(4, 26);
            this.tabLogs.Name = "tabLogs";
            this.tabLogs.Padding = new System.Windows.Forms.Padding(3);
            this.tabLogs.Size = new System.Drawing.Size(828, 309);
            this.tabLogs.TabIndex = 1;
            this.tabLogs.Text = "Work with logs";
            this.tabLogs.UseVisualStyleBackColor = true;
            // 
            // perflauncher
            // 
            this.perflauncher.Location = new System.Drawing.Point(348, 78);
            this.perflauncher.Name = "perflauncher";
            this.perflauncher.Size = new System.Drawing.Size(255, 36);
            this.perflauncher.TabIndex = 14;
            this.perflauncher.Text = "Calculate installation time Launcher\n";
            this.perflauncher.UseVisualStyleBackColor = true;
            this.perflauncher.Click += new System.EventHandler(this.launcherperf_Click);
            // 
            // geterrors
            // 
            this.geterrors.Location = new System.Drawing.Point(647, 78);
            this.geterrors.Name = "geterrors";
            this.geterrors.Size = new System.Drawing.Size(150, 36);
            this.geterrors.TabIndex = 13;
            this.geterrors.Text = "Get Errors";
            this.geterrors.UseVisualStyleBackColor = true;
            this.geterrors.Click += new System.EventHandler(this.geterrors_Click);
            // 
            // savefile
            // 
            this.savefile.Location = new System.Drawing.Point(647, 243);
            this.savefile.Name = "savefile";
            this.savefile.Size = new System.Drawing.Size(150, 36);
            this.savefile.TabIndex = 12;
            this.savefile.Text = "Save to File";
            this.savefile.UseVisualStyleBackColor = true;
            this.savefile.Click += new System.EventHandler(this.savefile_Click);
            // 
            // wgcLog
            // 
            this.wgcLog.AutoSize = true;
            this.wgcLog.Location = new System.Drawing.Point(216, 26);
            this.wgcLog.Name = "wgcLog";
            this.wgcLog.Size = new System.Drawing.Size(0, 17);
            this.wgcLog.TabIndex = 11;
            // 
            // perfbtn
            // 
            this.perfbtn.Location = new System.Drawing.Point(46, 78);
            this.perfbtn.Name = "perfbtn";
            this.perfbtn.Size = new System.Drawing.Size(255, 36);
            this.perfbtn.TabIndex = 10;
            this.perfbtn.Text = "Calculate installation time WGC\r\n";
            this.perfbtn.UseVisualStyleBackColor = true;
            this.perfbtn.Click += new System.EventHandler(this.perfbtn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(43, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "Select log file";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = global::wds_qa_tools.Properties.Resources.open;
            this.pictureBox2.Location = new System.Drawing.Point(151, 26);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(33, 27);
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // networktab
            // 
            this.networktab.Controls.Add(this.uploadspeedlabel);
            this.networktab.Controls.Add(this.label4);
            this.networktab.Controls.Add(this.downloadspeedlabel);
            this.networktab.Controls.Add(this.label3);
            this.networktab.Controls.Add(this.adapters);
            this.networktab.Controls.Add(this.label2);
            this.networktab.Controls.Add(this.turnonbtn);
            this.networktab.Controls.Add(this.turnoffbtn);
            this.networktab.Location = new System.Drawing.Point(4, 26);
            this.networktab.Name = "networktab";
            this.networktab.Padding = new System.Windows.Forms.Padding(3);
            this.networktab.Size = new System.Drawing.Size(828, 309);
            this.networktab.TabIndex = 2;
            this.networktab.Text = "Network";
            this.networktab.UseVisualStyleBackColor = true;
            // 
            // uploadspeedlabel
            // 
            this.uploadspeedlabel.AutoSize = true;
            this.uploadspeedlabel.Location = new System.Drawing.Point(524, 134);
            this.uploadspeedlabel.Name = "uploadspeedlabel";
            this.uploadspeedlabel.Size = new System.Drawing.Size(15, 17);
            this.uploadspeedlabel.TabIndex = 8;
            this.uploadspeedlabel.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(359, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 17);
            this.label4.TabIndex = 7;
            this.label4.Text = "Current Upload speed:";
            // 
            // downloadspeedlabel
            // 
            this.downloadspeedlabel.AutoSize = true;
            this.downloadspeedlabel.Location = new System.Drawing.Point(236, 134);
            this.downloadspeedlabel.Name = "downloadspeedlabel";
            this.downloadspeedlabel.Size = new System.Drawing.Size(15, 17);
            this.downloadspeedlabel.TabIndex = 6;
            this.downloadspeedlabel.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(71, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(162, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Current Download speed:";
            // 
            // adapters
            // 
            this.adapters.FormattingEnabled = true;
            this.adapters.Location = new System.Drawing.Point(74, 55);
            this.adapters.Name = "adapters";
            this.adapters.Size = new System.Drawing.Size(268, 25);
            this.adapters.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(71, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Network Interfaces";
            // 
            // turnonbtn
            // 
            this.turnonbtn.Location = new System.Drawing.Point(625, 43);
            this.turnonbtn.Name = "turnonbtn";
            this.turnonbtn.Size = new System.Drawing.Size(168, 47);
            this.turnonbtn.TabIndex = 1;
            this.turnonbtn.Text = "Turn On";
            this.turnonbtn.UseVisualStyleBackColor = true;
            this.turnonbtn.Click += new System.EventHandler(this.turnonbtn_Click);
            // 
            // turnoffbtn
            // 
            this.turnoffbtn.Location = new System.Drawing.Point(432, 43);
            this.turnoffbtn.Name = "turnoffbtn";
            this.turnoffbtn.Size = new System.Drawing.Size(171, 47);
            this.turnoffbtn.TabIndex = 0;
            this.turnoffbtn.Text = "Turn Off";
            this.turnoffbtn.UseVisualStyleBackColor = true;
            this.turnoffbtn.Click += new System.EventHandler(this.turnoffbtn_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.enablepreloadpatches);
            this.tabPage1.Controls.Add(this.custommock);
            this.tabPage1.Controls.Add(this.createversion);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.wgcbuild);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.wgcbranch);
            this.tabPage1.Controls.Add(this.wgcinfo);
            this.tabPage1.Controls.Add(this.ips);
            this.tabPage1.Controls.Add(this.disablemock);
            this.tabPage1.Controls.Add(this.startmock);
            this.tabPage1.Location = new System.Drawing.Point(4, 26);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(828, 309);
            this.tabPage1.TabIndex = 3;
            this.tabPage1.Text = "Mocks";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // enablepreloadpatches
            // 
            this.enablepreloadpatches.AutoSize = true;
            this.enablepreloadpatches.Location = new System.Drawing.Point(304, 37);
            this.enablepreloadpatches.Name = "enablepreloadpatches";
            this.enablepreloadpatches.Size = new System.Drawing.Size(164, 21);
            this.enablepreloadpatches.TabIndex = 10;
            this.enablepreloadpatches.Text = "Enable Preload patches";
            this.enablepreloadpatches.UseVisualStyleBackColor = true;
            // 
            // custommock
            // 
            this.custommock.Location = new System.Drawing.Point(656, 246);
            this.custommock.Name = "custommock";
            this.custommock.Size = new System.Drawing.Size(154, 41);
            this.custommock.TabIndex = 9;
            this.custommock.Text = "Add custom mock";
            this.custommock.UseVisualStyleBackColor = true;
            this.custommock.Click += new System.EventHandler(this.custommock_Click);
            // 
            // createversion
            // 
            this.createversion.Location = new System.Drawing.Point(654, 139);
            this.createversion.Name = "createversion";
            this.createversion.Size = new System.Drawing.Size(154, 44);
            this.createversion.TabIndex = 8;
            this.createversion.Text = "Create version.xml";
            this.createversion.UseVisualStyleBackColor = true;
            this.createversion.Click += new System.EventHandler(this.createversion_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 139);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 17);
            this.label7.TabIndex = 7;
            this.label7.Text = "Build";
            // 
            // wgcbuild
            // 
            this.wgcbuild.Location = new System.Drawing.Point(40, 159);
            this.wgcbuild.Name = "wgcbuild";
            this.wgcbuild.Size = new System.Drawing.Size(284, 25);
            this.wgcbuild.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(40, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "WGC Branch";
            // 
            // wgcbranch
            // 
            this.wgcbranch.Location = new System.Drawing.Point(40, 100);
            this.wgcbranch.Name = "wgcbranch";
            this.wgcbranch.Size = new System.Drawing.Size(284, 25);
            this.wgcbranch.TabIndex = 4;
            // 
            // wgcinfo
            // 
            this.wgcinfo.Location = new System.Drawing.Point(654, 89);
            this.wgcinfo.Name = "wgcinfo";
            this.wgcinfo.Size = new System.Drawing.Size(156, 44);
            this.wgcinfo.TabIndex = 3;
            this.wgcinfo.Text = "Create wgc_info.xml";
            this.wgcinfo.UseVisualStyleBackColor = true;
            this.wgcinfo.Click += new System.EventHandler(this.wgcinfo_Click);
            // 
            // ips
            // 
            this.ips.FormattingEnabled = true;
            this.ips.Location = new System.Drawing.Point(43, 35);
            this.ips.Name = "ips";
            this.ips.Size = new System.Drawing.Size(233, 25);
            this.ips.TabIndex = 2;
            // 
            // disablemock
            // 
            this.disablemock.Location = new System.Drawing.Point(656, 18);
            this.disablemock.Name = "disablemock";
            this.disablemock.Size = new System.Drawing.Size(154, 42);
            this.disablemock.TabIndex = 1;
            this.disablemock.Text = "Disable";
            this.disablemock.UseVisualStyleBackColor = true;
            this.disablemock.Click += new System.EventHandler(this.disablemock_Click);
            // 
            // startmock
            // 
            this.startmock.Location = new System.Drawing.Point(483, 18);
            this.startmock.Name = "startmock";
            this.startmock.Size = new System.Drawing.Size(156, 42);
            this.startmock.TabIndex = 0;
            this.startmock.Text = "Enable";
            this.startmock.UseVisualStyleBackColor = true;
            this.startmock.Click += new System.EventHandler(this.startmock_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.killp);
            this.tabPage2.Controls.Add(this.priorities);
            this.tabPage2.Controls.Add(this.pictureBox3);
            this.tabPage2.Controls.Add(this.setpriority);
            this.tabPage2.Controls.Add(this.label8);
            this.tabPage2.Controls.Add(this.procesess);
            this.tabPage2.Location = new System.Drawing.Point(4, 26);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(828, 309);
            this.tabPage2.TabIndex = 4;
            this.tabPage2.Text = "Process";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // killp
            // 
            this.killp.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.killp.Location = new System.Drawing.Point(600, 157);
            this.killp.Name = "killp";
            this.killp.Size = new System.Drawing.Size(178, 44);
            this.killp.TabIndex = 5;
            this.killp.Text = "Kill";
            this.killp.UseVisualStyleBackColor = true;
            this.killp.Click += new System.EventHandler(this.killp_Click);
            // 
            // priorities
            // 
            this.priorities.FormattingEnabled = true;
            this.priorities.Items.AddRange(new object[] {
            "Realtime",
            "High",
            "Above normal",
            "Normal",
            "Below normal",
            "Idle"});
            this.priorities.Location = new System.Drawing.Point(600, 44);
            this.priorities.Name = "priorities";
            this.priorities.Size = new System.Drawing.Size(178, 25);
            this.priorities.TabIndex = 4;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = global::wds_qa_tools.Properties.Resources.refresh;
            this.pictureBox3.Location = new System.Drawing.Point(377, 44);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(29, 25);
            this.pictureBox3.TabIndex = 3;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // setpriority
            // 
            this.setpriority.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.setpriority.Location = new System.Drawing.Point(600, 92);
            this.setpriority.Name = "setpriority";
            this.setpriority.Size = new System.Drawing.Size(178, 45);
            this.setpriority.TabIndex = 2;
            this.setpriority.Text = "Set Priority";
            this.setpriority.UseVisualStyleBackColor = true;
            this.setpriority.Click += new System.EventHandler(this.setpriority_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(39, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 17);
            this.label8.TabIndex = 1;
            this.label8.Text = "Process name, Pid";
            // 
            // procesess
            // 
            this.procesess.FormattingEnabled = true;
            this.procesess.Location = new System.Drawing.Point(42, 44);
            this.procesess.Name = "procesess";
            this.procesess.Size = new System.Drawing.Size(319, 25);
            this.procesess.TabIndex = 0;
            // 
            // output
            // 
            this.output.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.output.Location = new System.Drawing.Point(16, 357);
            this.output.Name = "output";
            this.output.Size = new System.Drawing.Size(835, 187);
            this.output.TabIndex = 1;
            this.output.Text = "";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 10000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(600, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Priority";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 556);
            this.Controls.Add(this.output);
            this.Controls.Add(this.mocks);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WDS QA Tools";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mocks.ResumeLayout(false);
            this.tabDisk.ResumeLayout(false);
            this.tabDisk.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabLogs.ResumeLayout(false);
            this.tabLogs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.networktab.ResumeLayout(false);
            this.networktab.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl mocks;
        private System.Windows.Forms.TabPage tabDisk;
        private System.Windows.Forms.TabPage tabLogs;
        private System.Windows.Forms.Label diskNameLabel;
        private System.Windows.Forms.Button fillSpace;
        public System.Windows.Forms.RichTextBox output;
        private System.Windows.Forms.Button freeSpace;
        private System.Windows.Forms.TextBox filePathToLock;
        private System.Windows.Forms.Button lockButton;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox checkBoxFile;
        private System.Windows.Forms.CheckBox checkBoxDir;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button buttonUnlock;
        private System.Windows.Forms.TabPage networktab;
        private System.Windows.Forms.Button turnoffbtn;
        private System.Windows.Forms.Button turnonbtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox adapters;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label downloadspeedlabel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label uploadspeedlabel;
        private System.Windows.Forms.CheckBox pathcontains;
        private System.Windows.Forms.TextBox containstextbox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Button perfbtn;
        private System.Windows.Forms.Label wgcLog;
        private System.Windows.Forms.Button savefile;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button geterrors;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button startmock;
        private System.Windows.Forms.Button disablemock;
        public System.Windows.Forms.ComboBox ips;
        private System.Windows.Forms.Button wgcinfo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox wgcbranch;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox wgcbuild;
        private System.Windows.Forms.Button createversion;
        private System.Windows.Forms.Button custommock;
        private System.Windows.Forms.Button perflauncher;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox procesess;
        private System.Windows.Forms.Button setpriority;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.ComboBox priorities;
        private System.Windows.Forms.Button killp;
        private System.Windows.Forms.CheckBox enablepreloadpatches;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox diskName;
        private System.Windows.Forms.Label label1;
    }
}

