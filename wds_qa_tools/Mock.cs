﻿using HttpMock;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using System.Xml.Linq;

namespace wds_qa_tools
{
    public class Mock
    {
        public static IHttpServer _stubHttp;
        private IRequestStub requestHandlerPatches;
        private IRequestStub requestHandlerIntegrity;
        private IRequestStub requestHandlerMetadata;
        private IRequestStub requestHandlerWGCPatches;
        private IRequestStub requestHandlerWGCMeta;
        private bool enablePreload;
        string webseed = "http://download.pershastudia.org/test_data/wgc_dummy_game/wot/patches/";

        private List<Dictionary<string, string>> get_patch_chains(string part, string version)
        {
            var chains = new Dictionary<string, List<Dictionary<string, string>>>{
                { "0", new List<Dictionary<string, string>> { new Dictionary<string, string> { { part, "1.1" } },  new Dictionary<string, string> { { part, "2.1-1.1" } } } } ,
                   { "1.1", new List<Dictionary<string, string>> { new Dictionary<string, string> { { part, "2.1-1.1" } } } },
                   { "2.1", new List<Dictionary<string, string>>() },
                   { "3.1", new List<Dictionary<string, string>>() }};
            return chains[version];
        }
        private List<Dictionary<string, string>> get_preload_patch_chains(string part, string version)
        {
            var chains = new Dictionary<string, List<Dictionary<string, string>>>{
                   { "2.1", new List<Dictionary<string, string>> { new Dictionary<string, string> { { part, "3.1-2.1" } } } },
                   { "3.1", new List<Dictionary<string, string>>() }};
            return chains[version];
        }
        private List<Dictionary<string, string>> get_integrity_chains(string part, string version)
        {
            var chains = new Dictionary<string, List<Dictionary<string, string>>>{
                { "0", new List<Dictionary<string, string>>() } ,
                   { "1.1", new List<Dictionary<string, string>> { new Dictionary<string, string> { { part, "1.1" } } } },
                   { "2.1",new List<Dictionary<string, string>> { new Dictionary<string, string> { { part, "2.1" } } } },
                   { "", new List<Dictionary<string, string>>() },
                   { "3.1", new List<Dictionary<string, string>>() }};
            return chains[version];
        }
        public string get_patchces()
        {
            var result = new List<Dictionary<string, string>>();
            var possibleVersions = new List<string> { "0", "1.1", "2.1" };
            var possibleParts = new List<string> { "minisd", "locale", "fullsd", "fullhd" };
            var metadata_version = "5000000000000";

            var request = ((RequestHandler)requestHandlerPatches).LastRequest();
            var queryStringPair = HttpUtility.ParseQueryString(request.RequestHead.QueryString);
            IDictionary<string, string> queryString = queryStringPair.Cast<string>().ToDictionary(p => p, p => queryStringPair[p]);
            string lang = queryString["lang"].ToLower();
            string patches_type = "install";
            foreach (string part in possibleParts)
            {
                string version = String.Format("{0}_current_version", part);
                if (queryString.ContainsKey(version))
                {
                    if (enablePreload && queryString[version] == "2.1")
                    {
                        result.AddRange(get_preload_patch_chains(part, queryString[version]));
                        if (result.Count > 0)
                            patches_type = "preload";
                    }
                    else
                    {
                        result.AddRange(get_patch_chains(part, queryString[version]));
                    }
                }
            }
            if (queryString["metadata_version"] != metadata_version)
                return generate_update_meta(true, "2.1");
            return generate_for_patch(result, "wot", "ru", lang, patches_type);
        }
        public string get_wgc_patchces()
        {
            Form currentForm = Form.ActiveForm;
            var result = new List<Dictionary<string, string>>();
            var possibleParts = new List<string> { "win32" };
            var metadata_version = "5000000000000";

            var request = ((RequestHandler)requestHandlerWGCPatches).LastRequest();
            var queryStringPair = HttpUtility.ParseQueryString(request.RequestHead.QueryString);
            IDictionary<string, string> queryString = queryStringPair.Cast<string>().ToDictionary(p => p, p => queryStringPair[p]);

            if (queryString["metadata_version"] != metadata_version)
                return generate_update_wgc_meta(true, MainForm.wgc_build);
            if (queryString["win32_current_version"] == MainForm.wgc_build)
                return generate_update_wgc_meta(false, MainForm.wgc_build);
            return generate_wgc_for_patch("ru");
        }
        public string get_integrity()
        {
            var result = new List<Dictionary<string, string>>();
            var part_lang = "ru";
            var possibleVersions = new List<string> { "0", "1.1", "2.1" };
            var possibleParts = new List<string> { "minisd", "locale", "fullsd", "fullhd" };

            var request = ((RequestHandler)requestHandlerIntegrity).LastRequest();
            var queryStringPair = HttpUtility.ParseQueryString(request.RequestHead.QueryString);
            IDictionary<string, string> queryString = queryStringPair.Cast<string>().ToDictionary(p => p, p => queryStringPair[p]);

            foreach (KeyValuePair<string, string> kvp in queryString)
            {
                if (kvp.Key.Contains("_lang"))
                {
                    part_lang = kvp.Value.ToLower();
                }
            }
            foreach (string part in possibleParts)
            {
                string version = String.Format("{0}_check_version", part);
                if (queryString.ContainsKey(version))
                {
                    result.AddRange(get_integrity_chains(part, queryString[version]));
                }
            }
            return generate_for_integrity(result, "wot", "ru", part_lang);
        }
        public string get_metadata()
        {
            string result;
            var request = ((RequestHandler)requestHandlerMetadata).LastRequest();
            var queryStringPair = HttpUtility.ParseQueryString(request.RequestHead.QueryString);
            IDictionary<string, string> queryString = queryStringPair.Cast<string>().ToDictionary(p => p, p => queryStringPair[p]);
            var app_id = queryString["guid"];
            var mutex = String.Format("{0}_client_mutex", app_id.Split('.')[0].ToLower());
            var metadata_version = "5000000000000";
            var filename = "wds_qa_tools.Resources.game_metadata5.0.xml";
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(filename))
            using (StreamReader reader = new StreamReader(stream))
            {
                result = String.Format(reader.ReadToEnd(), app_id, metadata_version, mutex);

            }
            return result;
        }
        public string get_wgc_metadata()
        {
            string result;
            var request = ((RequestHandler)requestHandlerWGCMeta).LastRequest();
            var queryStringPair = HttpUtility.ParseQueryString(request.RequestHead.QueryString);
            IDictionary<string, string> queryString = queryStringPair.Cast<string>().ToDictionary(p => p, p => queryStringPair[p]);
            var metadata_version = "5000000000000";
            var filename = "wds_qa_tools.Resources.wgc.metadata.xml";
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(filename))
            using (StreamReader reader = new StreamReader(stream))
            {
                result = String.Format(reader.ReadToEnd(), metadata_version);

            }
            return result;
        }
        private string generate_for_patch(List<Dictionary<string, string>> part_versions, string game, string region, string lang, string patches_type)
        {
            var _game = game.ToLower();
            var max_version = "2.1";
            var contentParts = new List<string> { "locale", "content" };
            var url = new XElement("url", webseed);
            url.SetAttributeValue("threads", 20);
            var pa = new XElement("patches_chain", new XAttribute("type", patches_type), new XElement("web_seeds", url), new XElement("meta_need_update", "False"), new XElement("private_ptp_enabled", "False"), new XElement("version_name", max_version));

            foreach (Dictionary<string, string> item in part_versions)
            {
                var p = new XElement("patch");
                foreach (KeyValuePair<string, string> kvp in item)
                {
                    var v = kvp.Value.Split('-');
                    string file, torrent;
                    torrent = String.Format("{0}{1}_{2}_{3}/{1}_{2}_{3}.torrent", webseed, _game, v[0], region);
                    Double m = Convert.ToDouble(v[0].Replace(".", ","));
                    if (v.Length == 1)
                    {
                        if (!contentParts.Contains(kvp.Key))
                        {
                            file = String.Format("{0}_{1}_{2}/{0}_{1}_{3}.wgpkg", _game, v[0], region, kvp.Key);
                        }
                        else
                        {
                            file = String.Format("{0}_{1}_{2}/{0}_{1}_{3}_{4}.wgpkg", _game, v[0], region, kvp.Key, lang);
                        }
                    }
                    else
                    {
                        if (!contentParts.Contains(kvp.Key))
                        {
                            file = String.Format("{0}_{4}_{2}/{0}_{4}_{1}_{3}.wgpkg", _game, v[1], region, kvp.Key, v[0]);
                        }
                        else
                        {
                            file = String.Format("{0}_{4}_{2}/{0}_{4}_{1}_{3}_{5}.wgpkg", _game, v[1], region, kvp.Key, v[0], lang);
                        }
                    }
                    p.Add(new XElement("files", new XElement("name", file)), new XElement("torrent", torrent),
                        new XElement("version_to", v[0]), new XElement("part", kvp.Key), new XElement("install", "True"));
                }
                pa.Add(p);
            }
            string protocol_version = "1.1";
            if (patches_type == "preload")
                protocol_version = "1.3";
            var doc = new XElement("protocol",
                new XAttribute("name", "patches_chain"), new XAttribute("version", protocol_version), pa);
            return doc.ToString();
        }
        private string generate_wgc_for_patch(string region)
        {
            string wgcwebseed = String.Format("http://download.pershastudia.org/wgc/demo/{0}/{1}/patch/", MainForm.wgc_branch.Replace("\\", "/"), MainForm.wgc_build);
            string file, torrent;
            var url = new XElement("url", wgcwebseed);
            url.SetAttributeValue("threads", 5);
            torrent = String.Format("{0}wgc_{1}_{2}/wgc_{1}_{2}.torrent", wgcwebseed, MainForm.wgc_build, region);
            file = String.Format("wgc_{0}_{1}/wgc_{0}_win32.wgpkg", MainForm.wgc_build, region);
            var doc = new XElement("protocol",
                new XAttribute("name", "patches_chain"), new XAttribute("version", "1.1"), new XElement("patches_chain", new XElement("patch", new XElement("files", new XElement("name", file)), new XElement("torrent", torrent),
                    new XElement("version_to", MainForm.wgc_build), new XElement("part", "win32"), new XElement("install", "True")),
                    new XElement("web_seeds", url), new XElement("meta_need_update", "False"), new XElement("private_ptp_enabled", "False"), new XElement("version_name", MainForm.wgc_build)));

            return doc.ToString();
        }
        private string generate_for_integrity(List<Dictionary<string, string>> part_versions, string game, string region, string lang)
        {
            var _game = game.ToLower();
            var contentParts = new List<string> { "locale", "content" };
            var url = new XElement("url", webseed);
            url.SetAttributeValue("threads", 20);
            var pp = new XElement("torrents");
            foreach (Dictionary<string, string> item in part_versions)
            {
                var p = new XElement("torrent");
                foreach (KeyValuePair<string, string> kvp in item)
                {
                    var v = kvp.Value.Split('-');
                    string torrent;
                    if (!contentParts.Contains(kvp.Key))
                    {
                        torrent = String.Format("{0}{1}_{2}_{3}/{1}_{2}_{4}_{3}.torrent", webseed, _game, v[0], region, kvp.Key);
                    }
                    else
                    {
                        torrent = String.Format("{0}{1}_{2}_{3}/{1}_{2}_{4}_{5}_{3}.torrent", webseed, _game, v[0], region, kvp.Key, lang);
                    }
                    p.Add(new XElement("file", torrent), new XElement("part", kvp.Key));
                }
                pp.Add(p);
                pp.Add(new XElement("private_ptp_enabled", "False"));
            }
            var doc = new XElement("protocol", new XAttribute("name", "integrity_check"), new XAttribute("version", "1.4"), pp);
            doc.Add(new XElement("web_seeds", url));
            return doc.ToString();
        }
        private string generate_update_meta(bool meta_need_update, string max_version)
        {
            var need_update = "True";
            if (!meta_need_update) need_update = "False";
            var url = new XElement("url", webseed);
            url.SetAttributeValue("threads", 20);
            var doc = new XElement("protocol",
        new XAttribute("name", "patches_chain"), new XAttribute("version", "1.1"),
        new XElement("patches_chain", new XElement("web_seeds", url), new XElement("meta_need_update", need_update), new XElement("private_ptp_enabled", "False"),
                new XElement("version_name", max_version)));
            return doc.ToString();
        }
        private string generate_update_wgc_meta(bool meta_need_update, string max_version)
        {
            string wgcwebseed = String.Format("http://download.pershastudia.org/wgc/demo/{0}/{1}/patch/", MainForm.wgc_branch.Replace("\\", "/"), MainForm.wgc_build);
            var need_update = "True";
            if (!meta_need_update) need_update = "False";
            var url = new XElement("url", wgcwebseed);
            url.SetAttributeValue("threads", 5);
            var doc = new XElement("protocol",
        new XAttribute("name", "patches_chain"), new XAttribute("version", "1.1"),
        new XElement("patches_chain", new XElement("web_seeds", url), new XElement("meta_need_update", need_update), new XElement("private_ptp_enabled", "False"),
                new XElement("version_name", max_version)));
            return doc.ToString();
        }
        public void enable_mock(string host, bool preload)
        {
            enablePreload = preload;
            if (_stubHttp == null)
                _stubHttp = HttpMockRepository.At(host);

            requestHandlerPatches = _stubHttp.Stub(x => x.Get("/api/v1/patches_chain/"));
            requestHandlerPatches.Return(get_patchces).AsXmlContent().OK();

            //requestHandlerDownloads = _stubHttp.Stub(x => x.Get("/download"));
            //requestHandlerDownloads.ReturnFile(download_file).OK();

            requestHandlerWGCPatches = _stubHttp.Stub(x => x.Get("/wgc/api/v1/patches_chain/"));
            requestHandlerWGCPatches.Return(get_wgc_patchces).AsXmlContent().OK();
            requestHandlerWGCMeta = _stubHttp.Stub(x => x.Get("/wgc/api/v1/metadata/"));
            requestHandlerWGCMeta.Return(get_wgc_metadata).AsXmlContent().OK();

            requestHandlerIntegrity = _stubHttp.Stub(x => x.Get("/api/v1/integrity_check/"));
            requestHandlerIntegrity.Return(get_integrity).AsXmlContent().OK();
            requestHandlerMetadata = _stubHttp.Stub(x => x.Get("/api/v1/metadata/"));
            requestHandlerMetadata.Return(get_metadata).AsXmlContent().OK();
        }
        public void disabled_mock()
        {
            _stubHttp.Dispose();
            _stubHttp = null;
        }
    }
}
