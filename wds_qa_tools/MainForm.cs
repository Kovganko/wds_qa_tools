﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using System.Net.Sockets;
using System.Management;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Net;
using System.Globalization;

namespace wds_qa_tools
{
    public partial class MainForm : Form
    {
        CheckBox lastChecked;
        private readonly SynchronizationContext synchronizationContext;
        private long _lastBytesRecevied;
        private long _lastBytesSent;
        private DateTime _lastReceivedMesurement;
        private DateTime _lastSentMesurement;
        private double downloadspeed;
        private double uploadspeed;
        private string wgcLogFile;
        private string app_version;
        public static string wgc_build;
        public static string wgc_branch;
        private Mock response_builder;
        private Dictionary<string, ProcessPriorityClass> priorities_map = new Dictionary<string, ProcessPriorityClass> {
            { "Realtime", ProcessPriorityClass.RealTime },
            { "High", ProcessPriorityClass.High },
            { "Above normal", ProcessPriorityClass.AboveNormal },
            { "Normal", ProcessPriorityClass.Normal },
            { "Below normal", ProcessPriorityClass.BelowNormal },
            { "Idle", ProcessPriorityClass.Idle }};

        IPv4InterfaceStatistics interfaceStats;
        public MainForm()
        {
            InitializeComponent();
            synchronizationContext = SynchronizationContext.Current;
            response_builder = new Mock();

        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            this.lastChecked = this.checkBoxFile;
            pictureBox2.AllowDrop = true;
            pictureBox2.DragEnter += pictureBox2_DragEnter;
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
            app_version = fvi.FileVersion;
            this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            this.pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;
            this.pictureBox3.SizeMode = PictureBoxSizeMode.StretchImage;
            this.ips.DropDownStyle = ComboBoxStyle.DropDownList;
            this.priorities.DropDownStyle = ComboBoxStyle.DropDownList;
            this.procesess.DropDownStyle = ComboBoxStyle.DropDownList;
            this.priorities.SelectedIndex = 3;
            this.wgcbranch.Text = "release\\17.01";
            this.wgcbuild.Text = "17.01.00.2069";
            foreach (DriveInfo item in DriveInfo.GetDrives())
            {
                this.diskName.Items.Add(item.Name);
            }
            this.diskName.SelectedIndex = 0;
            foreach (string item in getInetInterface())
            {
                this.adapters.Items.Add(item);
                this.adapters.SelectedIndex = 0;
            }
            foreach (string item in GetLocalIPAddress())
            {
                this.ips.Items.Add(item);
                this.ips.SelectedIndex = 0;
            }
            List<string> processNames = new List<string>();
            foreach (var p in Process.GetProcesses())
            {
                processNames.Add(p.ProcessName + " , " + p.Id.ToString());
            }
            processNames.Sort();
            foreach (var p in processNames)
            {
                procesess.Items.Add(p);
                procesess.SelectedIndex = 0;
            }
            interfaceStats = this.getNetworkInterfaceByName(this.adapters.SelectedItem.ToString()).GetIPv4Statistics();
            _lastBytesRecevied = interfaceStats.BytesReceived;
            _lastBytesSent = interfaceStats.BytesSent;
            _lastReceivedMesurement = DateTime.UtcNow;
            _lastSentMesurement = DateTime.UtcNow;
            timer1.Start();
            // hook up timer event
            timer1.Tick += timer1_Tick;

        }
        public void AppendOutput(string text)
        {
            synchronizationContext.Send(new SendOrPostCallback(o =>
            {
                this.output.Text += (string)o;
            }), text);

        }
        public void ClearOutput()
        {
            synchronizationContext.Send(new SendOrPostCallback(o =>
            {
                this.output.Text = (string)o;
            }), "");

        }
        private void fillSpace_Click(object sender, EventArgs e)
        {
            this.fillSpaceOnDisk(this.diskName.SelectedItem.ToString());
        }
        private long GetTotalFreeSpace(string driveName)
        {
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady && drive.Name == driveName.ToUpper())
                {
                    return drive.TotalFreeSpace;
                }
            }
            return -1;
        }
        private void fillSpaceOnDisk(string disk)
        {
            long total = this.GetTotalFreeSpace(disk);
            if (total == -1)
            {
                this.output.Text = "Please select correct DISK for this operation.";
                return;
            }
            string fillPath = Path.Combine(disk, "fillspace.txt");
            if (File.Exists(fillPath))
            {
                this.output.Text = String.Format("File {0} already exists, it will deleted and recreated.", fillPath);
                File.Delete(fillPath);

            }

            long left = Convert.ToInt64(ConfigurationManager.AppSettings["leftSpace"]);
            try
            {
                FileStream fs = new FileStream(fillPath, FileMode.CreateNew);
                fs.Seek(total - left, SeekOrigin.Begin);
                fs.WriteByte(0);
                fs.Close();
                this.output.Text = String.Format("Space on Disk {0} was filled.", disk);
            }
            catch (UnauthorizedAccessException)
            {
                this.output.Text = "WDS QA Tools has sufficient permissions. Open with administrator previlages.";
            }
            catch (IOException e)
            {
                this.output.Text = e.ToString();
            }
        }
        private void freeSpace_Click(object sender, EventArgs e)
        {
            string fillPath = Path.Combine(this.diskName.SelectedItem.ToString(), "fillspace.txt");
            if (File.Exists(fillPath))
            {
                this.output.Text = String.Format("File {0} successfully deleted. Space on disk was free.", fillPath);
                File.Delete(fillPath);

            }
        }
        private List<string> getInetInterface()
        {
            List<string> inet = new List<string>();
            NetworkInterface[] inets = NetworkInterface.GetAllNetworkInterfaces();
            for (int i = 0; i < inets.Length; i++)
            {
                inet.Add(inets[i].Name);
            }
            return inet;
        }
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (this.checkBoxFile.Checked)
            {
                if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    this.filePathToLock.Text = openFileDialog1.FileName;
                }
            }
            else
            {
                if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    this.filePathToLock.Text = folderBrowserDialog1.SelectedPath;
                }
            }
        }
        private void chk_Click(object sender, EventArgs e)
        {
            CheckBox activeCheckBox = sender as CheckBox;
            if (activeCheckBox != lastChecked && lastChecked != null) lastChecked.Checked = false;
            lastChecked = activeCheckBox.Checked ? activeCheckBox : null;
        }
        private async void lockButton_Click(object sender, EventArgs e)
        {
            bool is_dir = this.checkBoxDir.Checked;
            DateTime startTime = DateTime.Now;
            bool founded = true;
            string locked_path = this.filePathToLock.Text;
            string resultPath = locked_path;
            string[] files = new string[400];
            string[] dirs = new string[400];
            await Task.Run(() =>
            {
                if (is_dir)
                {
                    if (this.pathcontains.Checked)
                    {
                        if (Directory.Exists(this.filePathToLock.Text))
                        {
                            try
                            {
                                files = Directory.GetFiles(this.filePathToLock.Text, this.containstextbox.Text, SearchOption.AllDirectories);
                                dirs = Directory.GetDirectories(this.filePathToLock.Text, this.containstextbox.Text, SearchOption.AllDirectories);
                            }
                            catch (UnauthorizedAccessException ma)
                            {
                                AppendOutput(ma.Message);
                                AppendOutput("Firstly unlock selected target or not use Contains option.");
                                return;
                            }

                            if (files.Length == 0 && dirs.Length == 0)
                            {
                                Task.Run(() =>
                                {
                                    AppendOutput(String.Format("Path {0}{1} not exists. Wait 30 seconds until will created.\r\n",
                       this.filePathToLock.Text, this.containstextbox.Text));
                                });
                            }
                            while (files.Length == 0 && dirs.Length == 0)
                            {
                                Thread.Sleep(40);
                                if ((DateTime.Now - startTime).Seconds > 30)
                                {
                                    AppendOutput(String.Format("{0}\\{1} was not created.", this.filePathToLock.Text, this.containstextbox.Text));
                                    founded = false;
                                    break;
                                }
                                files = Directory.GetFiles(this.filePathToLock.Text, this.containstextbox.Text, SearchOption.AllDirectories);
                                dirs = Directory.GetDirectories(this.filePathToLock.Text, this.containstextbox.Text, SearchOption.AllDirectories);
                            }
                            if (founded)
                            {
                                if (files.Length > 0) resultPath = files[0];
                                if (dirs.Length > 0) resultPath = dirs[0];
                            }
                        }
                        else
                        {
                            AppendOutput(String.Format("Directory {0} was not exists. Select exists dir.", this.filePathToLock.Text));
                        }
                    }
                    else if (!Directory.Exists(this.filePathToLock.Text))
                    {
                        Task.Run(() => { AppendOutput(String.Format("Directory {0} not exists. Wait 25 seconds until will created.\r\n", this.filePathToLock.Text)); });

                        while (!Directory.Exists(this.filePathToLock.Text))
                        {
                            Thread.Sleep(40);
                            if ((DateTime.Now - startTime).Seconds > 30)
                            {
                                AppendOutput(String.Format("{0} was not created.", this.filePathToLock.Text));
                                founded = false;
                                break;
                            }
                        }
                    }

                }
                else
                {
                    if (!File.Exists(this.filePathToLock.Text))
                    {
                        Task.Run(() =>
                        {
                            string txt = String.Format("File {0} not exists. Wait 25 seconds until will created.\r\n", this.filePathToLock.Text);
                            AppendOutput(txt);
                        });

                        while (!File.Exists(this.filePathToLock.Text))
                        {
                            Thread.Sleep(40);
                            if ((DateTime.Now - startTime).Seconds > 30)
                            {
                                AppendOutput(String.Format("{0} was not created.", this.filePathToLock.Text));
                                founded = false;
                                break;
                            }
                        }
                    }
                }
            });

            if (founded)
            {
                if (Directory.Exists(resultPath))
                {
                    string adminUserName = Environment.UserName;// getting your adminUserName
                    DirectorySecurity ds = Directory.GetAccessControl(resultPath);
                    FileSystemAccessRule fsa = new FileSystemAccessRule(adminUserName, FileSystemRights.FullControl, AccessControlType.Deny);
                    ds.AddAccessRule(fsa);
                    Directory.SetAccessControl(resultPath, ds);
                    this.output.Text += String.Format("Directory {0} successfully locked\r\n", resultPath);
                }
                else if (File.Exists(resultPath))
                {
                    string adminUserName = Environment.UserName;// getting your adminUserName
                    FileSecurity ds = File.GetAccessControl(resultPath);
                    FileSystemAccessRule fsa = new FileSystemAccessRule(adminUserName, FileSystemRights.FullControl, AccessControlType.Deny);
                    ds.AddAccessRule(fsa);
                    File.SetAccessControl(resultPath, ds);
                    this.output.Text += String.Format("File {0} successfully locked\r\n", resultPath);
                }
            }

        }
        private void buttonUnlock_Click(object sender, EventArgs e)
        {
            string resultPath = this.filePathToLock.Text;
            if (!Directory.Exists(resultPath) && !File.Exists(resultPath))
            {
                this.output.Text += "Please select existing file or directory!";
                return;
            }
            ManagementObjectSearcher usersSearcher = new ManagementObjectSearcher(@"SELECT * FROM Win32_UserAccount");
            ManagementObjectCollection users = usersSearcher.Get();
            foreach (ManagementObject user in users)
            {
                string userName = user["Name"].ToString();
                if (Directory.Exists(resultPath))
                {
                    DirectorySecurity ds = Directory.GetAccessControl(resultPath);
                    FileSystemAccessRule fsa = new FileSystemAccessRule(userName, FileSystemRights.FullControl, AccessControlType.Deny);
                    ds.RemoveAccessRule(fsa);
                    Directory.SetAccessControl(resultPath, ds);
                }
                else if (File.Exists(resultPath))
                {
                    FileSecurity ds = File.GetAccessControl(resultPath);
                    FileSystemAccessRule fsa = new FileSystemAccessRule(userName, FileSystemRights.FullControl, AccessControlType.Deny);
                    ds.RemoveAccessRule(fsa);
                    File.SetAccessControl(resultPath, ds);
                }
            }
            this.output.Text = String.Format("{0} successfully unlocked\r\n", resultPath);
        }
        private bool isAdministrator()
        {
            bool isAdmin;
            try
            {
                WindowsIdentity user = WindowsIdentity.GetCurrent();
                WindowsPrincipal principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (UnauthorizedAccessException)
            {
                isAdmin = false;
            }
            catch (Exception)
            {
                isAdmin = false;
            }
            return isAdmin;
        }
        private void turnoffbtn_Click(object sender, EventArgs e)
        {
            if (!this.isAdministrator())
            {
                this.output.Text = "You do not have Administrator previlages.";
                return;
            }
            SelectQuery wmiQuery = new SelectQuery("SELECT * FROM Win32_NetworkAdapter WHERE NetConnectionId != NULL");
            ManagementObjectSearcher searchProcedure = new ManagementObjectSearcher(wmiQuery);
            foreach (ManagementObject item in searchProcedure.Get())
            {
                if (((string)item["NetConnectionId"]) == this.adapters.SelectedItem.ToString())
                {
                    item.InvokeMethod("Disable", null);
                    this.output.Text = String.Format("{0} interface was successfully disabled.", this.adapters.SelectedItem.ToString());


                }
            }
        }
        private void turnonbtn_Click(object sender, EventArgs e)
        {
            if (!this.isAdministrator())
            {
                this.output.Text = "You do not have Administrator previlages.";
                return;
            }
            SelectQuery wmiQuery = new SelectQuery("SELECT * FROM Win32_NetworkAdapter WHERE NetConnectionId != NULL");
            ManagementObjectSearcher searchProcedure = new ManagementObjectSearcher(wmiQuery);
            foreach (ManagementObject item in searchProcedure.Get())
            {
                if (((string)item["NetConnectionId"]) == this.adapters.SelectedItem.ToString())
                {
                    item.InvokeMethod("Enable", null);
                    this.output.Text = String.Format("{0} interface was successfully enabled.", this.adapters.SelectedItem.ToString());
                }
            }
        }
        private NetworkInterface getNetworkInterfaceByName(string name)
        {
            NetworkInterface[] inets = NetworkInterface.GetAllNetworkInterfaces();
            for (int i = 0; i < inets.Length; i++)
            {
                if (name == inets[i].Name)
                {
                    return inets[i];
                }
            }
            return null;
        }
        private void timer1_Tick(object sender, EventArgs e)
        {

            interfaceStats = this.getNetworkInterfaceByName(this.adapters.SelectedItem.ToString()).GetIPv4Statistics();
            double result = (interfaceStats.BytesReceived - _lastBytesRecevied) / (DateTime.UtcNow - _lastReceivedMesurement).TotalSeconds;

            _lastReceivedMesurement = DateTime.UtcNow;
            _lastBytesRecevied = interfaceStats.BytesReceived;
            downloadspeed = Math.Round(result / 1024d);

            double result2 = (interfaceStats.BytesSent - _lastBytesSent) / (DateTime.UtcNow - _lastSentMesurement).TotalSeconds;

            _lastSentMesurement = DateTime.UtcNow;
            _lastBytesSent = interfaceStats.BytesSent;

            uploadspeed = Math.Round(result2 / 1024d);

            downloadspeedlabel.Text = downloadspeed + " KB/s";
            uploadspeedlabel.Text = uploadspeed + " KB/s";
        }
        private void pathcontains_CheckedChanged(object sender, EventArgs e)
        {
            if (this.pathcontains.Checked)
            {
                this.containstextbox.Enabled = true;
            }
            else
            {
                this.containstextbox.Enabled = false;
            }
        }
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (this.openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.wgcLogFile = openFileDialog1.FileName;
                this.wgcLog.Text = openFileDialog1.FileName;
            }
        }
        private async void perfbtn_Click(object sender, EventArgs e)
        {
            ClearOutput();
            AppendOutput("Time calculated...\r\n");
            if (this.wgcLogFile != null)
            {
                await Task.Run(() =>
                {
                    try
                    {
                        var content = File.ReadAllText(this.wgcLogFile);
                        long unpackTime = 0;
                        long installTime = 0;
                        long downloadTime = 0;


                        List<string> packages = new List<string>();
                        Dictionary<string, List<int>> games = new Dictionary<string, List<int>>();

                        var download_s = Regex.Matches(content, @"(\d.+)\w:Request.+game_part_download_started.+application_id=(.*?)&.+completed successfully");
                        AppendOutput(String.Format("game_part_download_started: {0}\r\n", download_s.Count));
                        var download_f = Regex.Matches(content, @"(\d.+)\w:Request.+game_part_download_finished.+completed successfully");
                        AppendOutput(String.Format("game_part_download_finished: {0}\r\n", download_f.Count));

                        var unp_package = Regex.Matches(content, @"(\d.+)\w:Request.+game_part_unpack_started.+application_id=(.*?)&.+completed successfully");
                        AppendOutput(String.Format("game_part_unpack_started: {0}\r\n", unp_package.Count));
                        var unpack_f = Regex.Matches(content, @"(\d.+)\w:Request.+game_part_unpack_finished.+application_id=(.*?)&.+completed successfully");
                        AppendOutput(String.Format("game_part_unpack_finished: {0}\r\n", unpack_f.Count));
                        var install_s = Regex.Matches(content, @"(\d.+)\w:Request.+game_part_install_started.+completed successfully");
                        AppendOutput(String.Format("game_part_install_started: {0}\r\n", install_s.Count));
                        var install_f = Regex.Matches(content, @"(\d.+)\w:Request.+game_part_install_finished.+completed successfully");
                        AppendOutput(String.Format("game_part_install_finished: {0}\r\n\r\n", install_f.Count));
                        try
                        {
                            for (int i = 0; i < unp_package.Count; i++)
                            {
                                if (!games.ContainsKey(unpack_f[i].Groups[2].Value))
                                {
                                    games.Add(unpack_f[i].Groups[2].Value, new List<int> { i });
                                }
                                else
                                {
                                    games[unpack_f[i].Groups[2].Value].Add(i);
                                }
                            }
                            foreach (KeyValuePair<string, List<int>> kvp in games)
                            {
                                AppendOutput(String.Format("\r\n{0}\r\n\r\n", kvp.Key));
                                foreach (int index in kvp.Value)
                                {
                                    if (index < download_f.Count)
                                    {
                                        string v = download_f[index].Groups[1].Value;
                                        string v2 = download_s[index].Groups[1].Value;

                                        DateTime dt1 = new DateTime(2017, 02, Convert.ToInt16(v.Substring(0, 2)), Convert.ToInt16(v.Substring(2, 2)),
                                            Convert.ToInt16(v.Substring(4, 2)), Convert.ToInt16(v.Substring(6, 2)));
                                        DateTime dt2 = new DateTime(2017, 02, Convert.ToInt16(v2.Substring(0, 2)), Convert.ToInt16(v2.Substring(2, 2)),
                                            Convert.ToInt16(v2.Substring(4, 2)), Convert.ToInt16(v2.Substring(6, 2)));
                                        downloadTime += Convert.ToInt32(dt1.Subtract(dt2).TotalSeconds);
                                    }
                                }
                                foreach (int index in kvp.Value)
                                {
                                    if (index < unpack_f.Count)
                                    {
                                        string v = unpack_f[index].Groups[1].Value;
                                        string v2 = unp_package[index].Groups[1].Value;

                                        DateTime dt1 = new DateTime(2017, 02, Convert.ToInt16(v.Substring(0, 2)), Convert.ToInt16(v.Substring(2, 2)),
                                            Convert.ToInt16(v.Substring(4, 2)), Convert.ToInt16(v.Substring(6, 2)));
                                        DateTime dt2 = new DateTime(2017, 02, Convert.ToInt16(v2.Substring(0, 2)), Convert.ToInt16(v2.Substring(2, 2)),
                                            Convert.ToInt16(v2.Substring(4, 2)), Convert.ToInt16(v2.Substring(6, 2)));
                                        unpackTime += Convert.ToInt32(dt1.Subtract(dt2).TotalSeconds);
                                    }
                                }
                                foreach (int index in kvp.Value)
                                {
                                    if (index < install_f.Count)
                                    {
                                        string v = install_f[index].Groups[1].Value;
                                        string v2 = install_s[index].Groups[1].Value;
                                        DateTime dt1 = new DateTime(2017, 02, Convert.ToInt16(v.Substring(0, 2)), Convert.ToInt16(v.Substring(2, 2)),
                                            Convert.ToInt16(v.Substring(4, 2)), Convert.ToInt16(v.Substring(6, 2)));
                                        DateTime dt2 = new DateTime(2017, 02, Convert.ToInt16(v2.Substring(0, 2)), Convert.ToInt16(v2.Substring(2, 2)),
                                            Convert.ToInt16(v2.Substring(4, 2)), Convert.ToInt16(v2.Substring(6, 2)));
                                        installTime += Convert.ToInt32(dt1.Subtract(dt2).TotalSeconds);
                                    }
                                }

                                var total = unpackTime + installTime + downloadTime;
                                TimeSpan t0 = TimeSpan.FromSeconds(downloadTime);
                                TimeSpan t = TimeSpan.FromSeconds(unpackTime);
                                TimeSpan t2 = TimeSpan.FromSeconds(installTime);
                                TimeSpan t3 = TimeSpan.FromSeconds(total);
                                AppendOutput(String.Format("Download time: {0:D1}:{1:D2}:{2:D2}\r\nUnpack time: {3:D1}:{4:D2}:{5:D2}\r\nInstall time: {6:D1}:{7:D2}:{8:D2}\r\nTotal time installing: {9:D1}:{10:D2}:{11:D2}\r\n",
                                    t0.Hours, t0.Minutes, t0.Seconds, t.Hours, t.Minutes, t.Seconds, t2.Hours, t2.Minutes, t2.Seconds, t3.Hours, t3.Minutes, t3.Seconds));
                            }
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            AppendOutput("Sorry, but log was corrupted. ¯\\_(ツ)_/¯");
                        }
                    }
                    catch (IOException ioe)
                    {
                        AppendOutput(ioe.Message);
                    }

                });
            }
            else
            {
                this.output.Text = "Firstly select log file.";
            }
        }
        private void savefile_Click(object sender, EventArgs e)
        {
            if (this.saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StreamWriter file = File.CreateText(saveFileDialog1.FileName);
                file.Write(this.output.Text);
                file.Close();
            }
        }
        private async void geterrors_Click(object sender, EventArgs e)
        {
            if (this.wgcLogFile != null)
            {
                await Task.Run(() =>
                {
                    ClearOutput();
                    try
                    {
                        var content = File.ReadAllText(this.wgcLogFile);
                        var errors = Regex.Matches(content, @"(\d.+)E:(.+)");
                        for (int i = 0; i < errors.Count; i++)
                        {
                            AppendOutput(String.Format("{0}", errors[i].Groups[2].Value));
                        }
                    }
                    catch (IOException ioe)
                    {
                        AppendOutput(ioe.Message);
                    }
                }
                );
            }
            else
            {
                this.output.Text = "Firstly select log file.";
            }
        }
        private List<string> GetLocalIPAddress()
        {
            List<string> ips = new List<string>();
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    ips.Add(ip.ToString());
                }
            }
            return ips;
        }
        private async void startmock_Click(object sender, EventArgs e)
        {
            string host = String.Format("http://{0}:9191", this.ips.SelectedItem);
            wgc_build = this.wgcbuild.Text;
            wgc_branch = this.wgcbranch.Text;
            await Task.Run(() =>
            {
                ClearOutput();
                AppendOutput("Server is starting.");
                response_builder.enable_mock(host, enablepreloadpatches.Checked);
            });
            ClearOutput();
            AppendOutput(String.Format("Mock enabled at {0}", host));
            this.startmock.Enabled = false;
        }
        private void disablemock_Click(object sender, EventArgs e)
        {
            response_builder.disabled_mock();
            string host = String.Format("http://{0}:9191", this.ips.SelectedItem);
            this.output.Text = String.Format("Mock disabled at {0}", host);
            this.startmock.Enabled = true;
        }
        private void wgcinfo_Click(object sender, EventArgs e)
        {
            this.saveFileDialog1.FileName = "wgc_info";
            this.saveFileDialog1.Filter = "XML-File | *.xml";
            this.saveFileDialog1.AddExtension = true;
            if (this.saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string host = String.Format("http://{0}:9191/", this.ips.SelectedItem);
                string host_wgc = String.Format("http://{0}:9191/wgc/", this.ips.SelectedItem);
                string result;

                result = @"<?xml version=""1.0""?>
<wgc_info version=""1.0"">
<wgc_id>WGC.RU.PRODUCTION</wgc_id>
<update_url>{0}</update_url>
<statistics_service_url>http://wgusst-kis70.kdo.space</statistics_service_url>
<private_ptp_enabled>false</private_ptp_enabled >
<default_region>RU</default_region >
<regions>
     <region id=""RU"" name=""Russia"">
           <game id = ""WOT.RU.PRODUCTION"" update_url = ""{1}"" showcase_id = ""wot"" />
           <game id = ""WOWP.RU.PRODUCTION"" update_url = ""{1}"" showcase_id = ""wowp"" />
           <game id = ""WOWS.RU.PRODUCTION"" update_url = ""{1}"" showcase_id = ""wows"" />
     </region >
</regions >
</wgc_info > ";
                StreamWriter file = File.CreateText(saveFileDialog1.FileName);
                file.Write(String.Format(result, host_wgc, host));
                file.Close();
            }
        }
        private void createversion_Click(object sender, EventArgs e)
        {
            this.saveFileDialog1.FileName = "version";
            this.saveFileDialog1.Filter = "XML-File | *.xml";
            this.saveFileDialog1.AddExtension = true;
            if (this.saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string host = String.Format("http://{0}:9191/", this.ips.SelectedItem);
                string host_wgc = String.Format("http://{0}:9191/wgc/", this.ips.SelectedItem);
                string result = @"<?xml version=""1.0"" encoding=""utf-8""?>
  <protocol name=""wgc_update"" version=""1.0"">
       <agent_updater>
         <app_version>{0}</app_version>
            <version_name>{0}</version_name>
               <configuration>Final</configuration>
             </agent_updater>
           </protocol >";
                StreamWriter file = File.CreateText(saveFileDialog1.FileName);
                file.Write(String.Format(result, this.wgcbuild.Text));
                file.Close();
            }
        }
        private void custommock_Click(object sender, EventArgs e)
        {
            CustomMock mock_form = new CustomMock(this);
            mock_form.Show();
        }
        private async void launcherperf_Click(object sender, EventArgs e)
        {
            ClearOutput();
            AppendOutput("Time calculated...\r\n\r\n");
            if (this.wgcLogFile != null)
            {
                await Task.Run(() =>
                {
                    try
                    {
                        var content = File.ReadAllText(this.wgcLogFile);
                        long unpackTime = 0;
                        long installTime = 0;
                        long downloadTime = 0;

                        var dwn_package = Regex.Matches(content, @"(?'start'\d{2}\/\d+\/\d+ (\d+:)+\d+)[ \t]+Initializing P2P session[\s\S]*?(?'end'\d{2}\/\d+\/\d+ (\d+:)+\d+)[ \t]+Unpacking patches");
                        AppendOutput(String.Format("Download patches: {0}\r\n", dwn_package.Count));

                        var unp_package = Regex.Matches(content, @"(?'start'\d{2}\/\d+\/\d+ (\d+:)+\d+)[ \t]+extracting patch file[\s\S]*?(?'end'\d{2}\/\d+\/\d+ (\d+:)+\d+)[ \t]+patch file extracted");
                        AppendOutput(String.Format("Unpacking patches: {0}\r\n", unp_package.Count));

                        var install = Regex.Matches(content, @"(?'start'\d{2}\/\d+\/\d+ (\d+:)+\d+)[ \t]+Installing patches[\s\S]*?(?'end'\d{2}\/\d+\/\d+ (\d+:)+\d+)[ \t]+Update complete. part:");
                        AppendOutput(String.Format("Installing patches: {0}\r\n", install.Count));

                        for (int index = 0; index < unp_package.Count; index++)
                        {
                            string v = unp_package[index].Groups[4].Value;
                            string v2 = unp_package[index].Groups[3].Value;

                            DateTime dt1 = DateTime.ParseExact(v, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                            DateTime dt2 = DateTime.ParseExact(v2, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                            unpackTime += Convert.ToInt32(dt1.Subtract(dt2).TotalSeconds);
                        }
                        for (int index = 0; index < dwn_package.Count; index++)
                        {
                            string v = dwn_package[index].Groups[4].Value;
                            string v2 = dwn_package[index].Groups[3].Value;

                            DateTime dt1 = DateTime.ParseExact(v, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                            DateTime dt2 = DateTime.ParseExact(v2, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                            downloadTime += Convert.ToInt32(dt1.Subtract(dt2).TotalSeconds);
                        }
                        for (int index = 0; index < install.Count; index++)
                        {

                            string v = install[index].Groups[4].Value;
                            string v2 = install[index].Groups[3].Value;
                            DateTime dt1 = DateTime.ParseExact(v, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                            DateTime dt2 = DateTime.ParseExact(v2, "dd/MM/yyyy HH:mm:ss", CultureInfo.CurrentCulture);
                            installTime += Convert.ToInt32(dt1.Subtract(dt2).TotalSeconds);
                        }

                        var total = unpackTime + installTime + downloadTime;
                        TimeSpan t0 = TimeSpan.FromSeconds(downloadTime);
                        TimeSpan t = TimeSpan.FromSeconds(unpackTime);
                        TimeSpan t2 = TimeSpan.FromSeconds(installTime);
                        TimeSpan t3 = TimeSpan.FromSeconds(total);
                        AppendOutput(String.Format("\r\nDownload time: {9:D1}:{10:D2}:{11:D2}\r\nUnpack time: {0:D1}:{1:D2}:{2:D2}\r\nInstall time: {3:D1}:{4:D2}:{5:D2}\r\nTotal time installing: {6:D1}:{7:D2}:{8:D2}\r\n",
                            t.Hours, t.Minutes, t.Seconds, t2.Hours, t2.Minutes, t2.Seconds, t3.Hours, t3.Minutes, t3.Seconds, t0.Hours, t0.Minutes, t0.Seconds));
                    }
                    catch (IOException ioe)
                    {
                        AppendOutput(ioe.Message);
                    }
                });
            }
            else
            {
                this.output.Text = "Firstly select log file.";
            }
        }
        private void setpriority_Click(object sender, EventArgs e)
        {
            int pid = Convert.ToInt32(procesess.SelectedItem.ToString().Split(',')[1]);
            string name = procesess.SelectedItem.ToString().Split(',')[0];
            using (Process p = Process.GetProcessById(pid))
                p.PriorityClass = priorities_map[priorities.SelectedItem.ToString()];
            this.output.Text = String.Format("Process {0} priorities was changed to {1}.", name, priorities.SelectedItem.ToString());
        }
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            procesess.Items.Clear();
            List<string> processNames = new List<string>();
            foreach (var p in Process.GetProcesses())
            {
                processNames.Add(p.ProcessName + " , " + p.Id.ToString());
            }
            processNames.Sort();
            foreach (var p in processNames)
            {
                procesess.Items.Add(p);
                procesess.SelectedIndex = 0;
            }
            this.output.Text = "Processes updated.";
        }
        private void killp_Click(object sender, EventArgs e)
        {
            int pid = Convert.ToInt32(procesess.SelectedItem.ToString().Split(',')[1]);
            string name = procesess.SelectedItem.ToString().Split(',')[0];
            using (Process p = Process.GetProcessById(pid))
                p.Kill();
            this.output.Text = String.Format("Process {0} was terminated.", name);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            string version_txt = "http://download.pershastudia.org/wot/patches/kovganko1/wds_qa_tools/version.txt";
            string version = null;
            using (var client = new WebClient())
            {
                try
                {
                    version = client.DownloadString(version_txt);
                }
                catch (WebException)
                {

                }
            }
            if (version != null && version != app_version)
            {
                Process.Start("updater.exe");
            }
        }

        void pictureBox2_DragEnter(object sender, DragEventArgs e)
        {
            var files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            this.wgcLogFile = files[0];
            this.wgcLog.Text = files[0];
        }
    }
}
