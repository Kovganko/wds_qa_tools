﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace wds_qa_tools.helpers
{
    public class GamePatch
    {
        public string create_game_patch(List<string> patchFiles, string patchPath, string gameID, List<string> 
            filesToDelete, bool addServiceXML, string volumeSize, string plugin, string chainIDToChange, string partToUpdate)
        {
            return "";
        }
        public static Dictionary<string, string> pluginHook()
        {
            return new Dictionary<string, string> { { "filename", "wg_hook_client_patch.dll" }, { "version", "1.0" } , { "weight", "20" } };
        }
        public static void create_service_xml(string folder, string gameID, List<string> files_to_delete)
        {
            string service_xml_output = Path.Combine(folder, "service.xml");
            string service_xml_input = @"<?xml version='1.0' encoding='UTF-8'?><patch_service_info><guid>{0}</guid><files_to_delete/></patch_service_info>";
            XDocument xdoc = new XDocument();
            xdoc = XDocument.Parse(String.Format(service_xml_input, gameID));
            if (files_to_delete.Count > 0)
            {
                var deleted = xdoc.Descendants("files_to_delete").First();
                foreach (string item in files_to_delete)
                {
                    deleted.Add(new XElement("file", item));
                }
            }
            if (File.Exists(service_xml_output))
                File.Delete(service_xml_output);
            xdoc.Save(service_xml_output);
        }
        public static void create_service_xml(string folder, string gameID, List<string> files_to_delete, Dictionary<string, string> plugin)
        {
            string service_xml_output = Path.Combine(folder, "service.xml");
            string service_xml_input = @"<?xml version='1.0' encoding='UTF-8'?><patch_service_info><guid>{0}</guid><files_to_delete/></patch_service_info>";
            XDocument xdoc = new XDocument();
            xdoc = XDocument.Parse(String.Format(service_xml_input, gameID));
            if (files_to_delete.Count > 0)
            {
                var deleted = xdoc.Descendants("files_to_delete").First();
                foreach (string item in files_to_delete)
                {
                    deleted.Add(new XElement("file", item));
                }
            }
            if (plugin.Count > 0)
            {
                var patch_service_info = xdoc.Descendants("patch_service_info").First();
                patch_service_info.Add(new XElement("plugins", new XElement("plugin",
                    new XElement("filename", plugin["filename"]), new XElement("version", plugin["version"]), new XElement("weight", plugin["weight"]))));
            }
            if (File.Exists(service_xml_output))
                File.Delete(service_xml_output);
            xdoc.Save(service_xml_output);
        }

    }
}
