﻿using HttpMock;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using wds_qa_tools;

namespace wds_qa_tools
{
    public partial class CustomMock : Form
    {
        private readonly MainForm _form1;
        public CustomMock(MainForm form1)
        {
            InitializeComponent();
            _form1 = form1;
        }
        private void addmock_Click(object sender, EventArgs e)
        {
            string host = String.Format("http://{0}:9191", _form1.ips.SelectedItem);
            if (Mock._stubHttp == null)
            {
                Mock._stubHttp = HttpMockRepository.At(host);
            }
            if (methods.SelectedItem.ToString() == "GET")
            {
                Mock._stubHttp.Stub(x => x.Get(this.custompath.Text)).Return(
                    this.customcontent.Text).AsContentType(contenttypes.SelectedItem.ToString()).OK();
            }
            if (methods.SelectedItem.ToString() == "POST")
            {
                Mock._stubHttp.Stub(x => x.Post(this.custompath.Text)).Return(
                    this.customcontent.Text).AsContentType(contenttypes.SelectedItem.ToString()).OK();
            }
            if (methods.SelectedItem.ToString() == "DELETE")
            {
                Mock._stubHttp.Stub(x => x.Delete(this.custompath.Text)).Return(
                    this.customcontent.Text).AsContentType(contenttypes.SelectedItem.ToString()).OK();
            }
            if (methods.SelectedItem.ToString() == "PUT")
            {
                Mock._stubHttp.Stub(x => x.Put(this.custompath.Text)).Return(
                    this.customcontent.Text).AsContentType(contenttypes.SelectedItem.ToString()).OK();
            }

            this._form1.AppendOutput(String.Format("Add custom mock for {0}{1}\r\n", host, this.custompath.Text));
            this.Close();
        }
        private void CustomMock_Load(object sender, EventArgs e)
        {
            methods.SelectedIndex = 0;
            contenttypes.SelectedIndex = 0;
        }
    }
}
